CREATE DATABASE IF NOT EXISTS `dgss1617_letta_teamA`
DEFAULT CHARACTER SET = utf8
DEFAULT COLLATE utf8_general_ci;

USE `dgss1617_letta_teamA`;

--
-- User creation
--
GRANT ALL PRIVILEGES ON dgss1617_letta_teamA.* TO letta@localhost IDENTIFIED BY 'lettapass';
FLUSH PRIVILEGES;

